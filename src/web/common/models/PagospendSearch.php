<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CondicionPago;

/**
 * PagospendSearch represents the model behind the search form about `common\models\CondicionPago`.
 */
class PagospendSearch extends CondicionPago 
{
    public $search;

    /**
     * @inheritdoc
     */
    
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(),['marca'], ['producto'],['year']);
       
    }
    public function rules()
    {
        return [
            [['id_cuota', 'id_factura', 'porcentaje', 'dias_credito', 'id_estatus_pago'], 'integer'],
            [['monto_estimado_pago'], 'number'],
            [['fecha_estimada_pago', 'fecha_pago','search','year'], 'safe'],
            [['marca','producto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
         $query = CondicionPago::find()
         
         ->select('marca.descripcion AS marca,tipo_producto.descripcion AS producto,
        sum(case when factura.id_mes=1 then condicion_pago.monto_estimado_pago end )AS Enero,
        sum(case when factura.id_mes=2 then condicion_pago.monto_estimado_pago end )AS Febrero,
        sum(case when factura.id_mes=3 then condicion_pago.monto_estimado_pago end )AS Marzo,
        sum(case when factura.id_mes=4 then condicion_pago.monto_estimado_pago end )AS Abril,
        sum(case when factura.id_mes=5 then condicion_pago.monto_estimado_pago end )AS Mayo,
        sum(case when factura.id_mes=6 then condicion_pago.monto_estimado_pago end )AS Junio,
        sum(case when factura.id_mes=7 then condicion_pago.monto_estimado_pago end )AS Julio,
        sum(case when factura.id_mes=8 then condicion_pago.monto_estimado_pago end )AS Agosto,
        sum(case when factura.id_mes=9 then condicion_pago.monto_estimado_pago end )AS Septiembre,
        sum(case when factura.id_mes=10 then condicion_pago.monto_estimado_pago end )AS Octubre,
        sum(case when factura.id_mes=11 then condicion_pago.monto_estimado_pago end )AS Noviembre,
        sum(case when factura.id_mes=12 then condicion_pago.monto_estimado_pago end )AS Diciembre')
        ->innerJoin('factura','factura.id_factura=condicion_pago.id_factura')
        ->innerJoin('marca','factura.id_marca=marca.id_marca')
        ->innerJoin('tipo_producto','factura.id_tipo_producto=tipo_producto.id_tipo_producto')
        ->where([
               'condicion_pago.id_estatus_pago' => [1,2],
               'year(condicion_pago.fecha_estimada_pago)' => date('Y'),
                ])
        ->groupBy('factura.id_marca,factura.id_tipo_producto');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 20,
            ],
        ]);
        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
    $query->andFilterWhere([
            'id_cuota' => $this->id_cuota,
            'id_factura' => $this->id_factura,
            'monto_estimado_pago' => $this->monto_estimado_pago,
            'fecha_estimada_pago' => $this->fecha_estimada_pago,
            'porcentaje' => $this->porcentaje,
            'dias_credito' => $this->dias_credito,
            'fecha_pago' => $this->fecha_pago,
            'id_estatus_pago' => $this->id_estatus_pago,
            'marca.descripcion' => $this->marca,
            'tipo_producto.descripcion'=> $this->producto,
        ]);


          $query->andFilterWhere(['like', 'marca.descripcion', $this->search]);
          $query->orFilterWhere(['like', 'producto.descripcion', $this->search]);
          $query->andFilterWhere(['like', 'year(condicion_pago.fecha_estimada_pago)', $this->year]);
        
        return $dataProvider;
    }
}
