<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class FacturaCuotaForm extends Model
{
    public $porcentajecuota;
    public $montocuota;
    public $diascredito;
    public $fechavencimiento;
    public $totalfactura;
    public $idfactura;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['porcentajecuota', 'montocuota', 'diascredito', 'fechavencimiento'], 'required'],
            // email has to be a valid email address
            ['fechavencimiento', 'date'],
            ['diascredito', 'integer'],
            ['montocuota,totalfactura', 'double'],
            // verifyCode needs to be entered correctly
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'totalfactura' => 'Total Factura',
        ];
    }

}
