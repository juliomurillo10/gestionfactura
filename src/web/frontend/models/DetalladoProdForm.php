<?php
namespace frontend\models;

use yii\base\Model;

/**
 * Detallado Produccion Form
 * @author Yusmary López <ylopez@grudu.org>
 */
class DetalladoProdForm extends Model
{
    public $marca;
    public $year;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marca','year'], 'required'],
        ];
    }
    
        
    /**
     * @return listado con años
     */   
    public function getYearsList() 
    {
        $currentYear = date('Y');
        $yearFrom = 2000;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }

}
