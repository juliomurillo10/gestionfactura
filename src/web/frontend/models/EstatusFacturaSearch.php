<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class EstatusFacturaSearch extends EstatusFactura
{
    public function rules()
    { 
        // only fields in rules() are searchable
        return [
            [['cod_estatus', 'descripcion'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Método para busqueda del filtro gridview
     * @param type $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query        = EstatusFactura::find()->where(['not', ['id_estatus' => null]]) ;
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'cod_estatus' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]) ;
        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider ;
        }

        // adjust the query by adding the filters
        
        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
                ->andFilterWhere(['like', 'cod_estatus', $this->cod_estatus]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'cod_estatus' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]) ;
        return $dataProvider ;
    }

}