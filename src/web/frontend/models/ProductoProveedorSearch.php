<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class ProductoProveedorSearch extends ProductoProveedor
{
    public function rules()
    { 
        // only fields in rules() are searchable
        return [
            [['id_proveedor', 'id_tipo_producto','idProveedor','idTipoProducto'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Método para busqueda del filtro gridview
     * @param type $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query        = ProductoProveedor::find()
                            ->joinWith(['idProveedor']);
//                            ->joinWith('IdTipoProducto','tipo_producto.id_tipo_producto = producto_proveedor.id_tipo_producto');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]) ;
        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider ;
        }

        // adjust the query by adding the filters
        
        $query->andFilterWhere(['like', 'id_proveedor ', $this->id_proveedor])
                ->andFilterWhere(['like', 'idProveedor.nombre ', $this->proveedor]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]) ;
        return $dataProvider ;
    }

}