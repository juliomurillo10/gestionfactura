<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Factura Status form
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class FacturaStatusForm extends Model
{
    public $desde;
    public $hasta;
    public $marca;
    public $estatus;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desde','hasta','marca','estatus'], 'required'],
            ['desde','validateDates'],
        ];
    }

    /**
     * Validate Dates
     */
    public function validateDates(){
        if(strtotime($this->hasta) <= strtotime($this->desde)){
            $this->addError('desde','Colocar correctos fecha desde y hasta');
            $this->addError('hasta','Colocar correctos fecha desde y hasta');
        }
    }
}
