<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Actualizar Tipo de Producto';
$this->params['breadcrumbs'][] = ['label' => 'Producto por proveedor','url' => Url::to(['producto-proveedor/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
        <?php $form = ActiveForm::begin() ; ?>
        
        <?= $form->field($model, 'id_proveedor')->dropDownList($dataProveedor) ?>
        <?= $form->field($model, 'id_tipo_producto')->dropDownList($dataTipo) ?>
        
        <div class="pull-left">
            <?= Html::a("Atrás", Url::to(['producto-proveedor/index']) ,['class' => 'btn btn-danger btn-flat'])?>
        </div>
        <div class="pull-right">
            <?= Html::submitButton("Editar", ['class' => 'btn btn-success btn-flat'])?>
        </div>
        
        <?php ActiveForm::end() ?>
    </div>
</div>