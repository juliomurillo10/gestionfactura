<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = 'Producto por proveedor';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
        <div class="pull-right">
            <?= Html::button('Agregar Producto por proveedor',['class'=>'btn btn-success btn-flat','data-toggle'=>'modal','data-target'=>'#tipoModal']) ?>
        </div>

        <?= 
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'header' => 'Nombre de Proveedor',
                        'attribute' => 'idProveedor.nombre',
                    ],
                    [
                        'header' => 'Descripción de tipo de Producto',
                        'attribute' => 'idTipoProducto.descripcion',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]);
        ?>
        
    </div>
    <div class="modal fade" id="tipoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Tipo de producto</h4>
                </div>
                <?php $form = ActiveForm::begin() ; ?>
                <div class="modal-body">
                    <p>
                        Agregar datos del tipo de producto
                    </p>
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
                            <?= $form->field($model, 'id_proveedor')->dropDownList($dataProveedor) ?>
                            <?= $form->field($model, 'id_tipo_producto')->dropDownList($dataTipo) ?>
                        </div>
                    </div>
                <div class="modal-footer">
                    <?= Html::submitButton("Agregar", ['class' => 'btn btn-success btn-flat'])?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>

</div>


