<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Actualizar Estatus de Factura';
$this->params['breadcrumbs'][] = ['label' => 'Estatus de Factura','url' => Url::to(['proveedor/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
        <?php $form = ActiveForm::begin() ; ?>
        
        <?= $form->field($model, 'cod_estatus')->textInput() ?>
        <?= $form->field($model, 'descripcion')->textInput() ?>
        
        <div class="pull-left">
            <?= Html::a("Atrás", Url::to(['estatus-factura/index']) ,['class' => 'btn btn-danger btn-flat'])?>
        </div>
        <div class="pull-right">
            <?= Html::submitButton("Editar", ['class' => 'btn btn-success btn-flat'])?>
        </div>
        
        <?php ActiveForm::end() ?>
    </div>
</div>