<?php

namespace app\maintenance\controllers;

use Yii;
use yii\web\Controller;
use app\models\ProductoProveedorSearch;
use app\models\ProductoProveedor;
use yii\helpers\Url;
use app\models\Proveedor;
use yii\helpers\ArrayHelper;
use app\models\TipoProducto;

/**
 * Default controller for the `maintenance` module
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class ProductoProveedorController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model        = new ProductoProveedor ;
        $searchModel  = new ProductoProveedorSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->get()) ;

        $proveedor     = Proveedor::find()->all() ;
        $dataProveedor = ArrayHelper::map($proveedor, 'id_proveedor', 'nombre') ;
        $tipoProducto  = TipoProducto::find()->all() ;
        $dataTipo      = ArrayHelper::map($tipoProducto, 'id_tipo_producto', 'descripcion') ;

        if (Yii::$app->request->isPost) {

            $model->load(Yii::$app->request->post()) ;
            if ($model->validate()) {
                $model->save() ;
                Yii::$app->session->setFlash('success', "Producto de Proveedor Guardado") ;
            } else {
                Yii::$app->session->setFlash('error', "Producto de Proveedor no Guardado") ;
            }
        }

        return $this->render('index', [
                    'dataProvider'  => $dataProvider,
                    'searchModel'   => $searchModel,
                    'model'         => $model,
                    'dataProveedor' => $dataProveedor,
                    'dataTipo'      => $dataTipo,
                ]) ;
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdate() {
        $idproveedor = Yii::$app->request->get('id_proveedor') ;
        $idtipo      = Yii::$app->request->get('id_tipo_producto') ;
        $model       = ProductoProveedor::find()->where(['id_proveedor' => $idproveedor])->andWhere(['id_tipo_producto' => $idtipo])->one() ;

        $proveedor     = Proveedor::find()->all() ;
        $dataProveedor = ArrayHelper::map($proveedor, 'id_proveedor', 'nombre') ;
        $tipoProducto  = TipoProducto::find()->all() ;
        $dataTipo      = ArrayHelper::map($tipoProducto, 'id_tipo_producto', 'descripcion') ;
        
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Producto de Proveedor Actualizado");
                
                return $this->redirect(Url::to(['producto-proveedor/index']));
            }else{
                Yii::$app->session->setFlash('error', "Producto de Proveedor no Actualizado");
            }
        }
        
        return $this->render('update', [
                    'model'  => $model,
                    'dataProveedor' => $dataProveedor,
                    'dataTipo'      => $dataTipo,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $model = ProductoProveedor::findOne($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "Producto de Proveedor Eliminado");
                
            return $this->redirect(Url::to(['producto-proveedor/index']));
        }else{
            Yii::$app->session->setFlash('error', "Producto de Proveedor no Eliminado");
        }
    }
}
