<?php

namespace app\maintenance\controllers;

use Yii;
use yii\web\Controller;
use app\models\TipoProductoSearch;
use app\models\TipoProducto;
use yii\helpers\Url;

/**
 * Default controller for the `maintenance` module
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class TipoProductoController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model        = new TipoProducto ;
        $searchModel  = new TipoProductoSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->get()) ;
        
        if (Yii::$app->request->isPost) {
            
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Tipo de Producto Guardado");
            }else{
                Yii::$app->session->setFlash('error', "Tipo de Producto no Guardado");
            }
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel'  => $searchModel,
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdate($id) {
        $model = TipoProducto::findOne($id);
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Tipo de Producto Actualizado");
                
                return $this->redirect(Url::to(['tipo-producto/index']));
            }else{
                Yii::$app->session->setFlash('error', "Tipo de Producto no Actualizado");
            }
        }
        
        return $this->render('update', [
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $model = TipoProducto::findOne($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "Tipo de Producto Eliminado");
                
            return $this->redirect(Url::to(['tipo-producto/index']));
        }else{
            Yii::$app->session->setFlash('error', "Tipo de Producto no Eliminado");
        }
    }
}
