<?php

namespace app\maintenance\controllers;

use Yii;
use yii\web\Controller;
use app\models\EstatusFacturaSearch;
use app\models\EstatusFactura;
use yii\helpers\Url;

/**
 * Default controller for the `maintenance` module
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class EstatusFacturaController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model        = new EstatusFactura ;
        $searchModel  = new EstatusFacturaSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->get()) ;
        
        if (Yii::$app->request->isPost) {
            
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Estatus de Factura Guardada");
            }else{
                Yii::$app->session->setFlash('error', "Estatus de Factura no Guardada");
            }
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel'  => $searchModel,
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdate($id) {
        $model = EstatusFactura::findOne($id);
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Estatus de Factura Actualizada");
                
                return $this->redirect(Url::to(['estatus-factura/index']));
            }else{
                Yii::$app->session->setFlash('error', "Estatus de Factura no Actualizada");
            }
        }
        
        return $this->render('update', [
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $model = EstatusFactura::findOne($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "Estatus de Factura Eliminada");
                
            return $this->redirect(Url::to(['estatus-factura/index']));
        }else{
            Yii::$app->session->setFlash('error', "Estatus de Factura no Eliminada");
        }
    }
}
