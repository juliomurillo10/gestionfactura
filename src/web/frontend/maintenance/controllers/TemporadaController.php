<?php

namespace app\maintenance\controllers;

use Yii;
use yii\web\Controller;
use app\models\TemporadaSearch;
use app\models\Temporada;
use yii\helpers\Url;

/**
 * Default controller for the `maintenance` module
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class TemporadaController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model        = new Temporada ;
        $searchModel  = new TemporadaSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->get()) ;
        
        if (Yii::$app->request->isPost) {
            
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Temporada Guardada");
            }else{
                Yii::$app->session->setFlash('error', "Temporada no Guardada");
            }
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel'  => $searchModel,
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdate($id) {
        $model = Temporada::findOne($id);
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Temporada Actualizada");
                
                return $this->redirect(Url::to(['temporada/index']));
            }else{
                Yii::$app->session->setFlash('error', "Temporada no Actualizada");
            }
        }
        
        return $this->render('update', [
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $model = Temporada::findOne($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "Temporada Eliminada");
                
            return $this->redirect(Url::to(['temporada/index']));
        }else{
            Yii::$app->session->setFlash('error', "Temporada no Eliminada");
        }
    }
}
