<?php

namespace app\maintenance\controllers;

use yii\web\Controller;
use Yii;
use app\models\ProveedorQuery;
use app\models\Proveedor;
use app\models\ProveedorSearch;
use yii\helpers\Url;

/**
 * Default controller for the `maintenance` module
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class ProveedorController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model        = new Proveedor ;
        $searchModel  = new ProveedorSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->get()) ;
        
        if (Yii::$app->request->isPost) {
            
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Proveedor Guardado");
            }else{
                Yii::$app->session->setFlash('error', "Proveedor no Guardado");
            }
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel'  => $searchModel,
                    'model'  => $model,
        ]) ;
    }
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdate($id) {
        $model = Proveedor::findOne($id);
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success', "Proveedor Actualizado");
                
                return $this->redirect(Url::to(['proveedor/index']));
            }else{
                Yii::$app->session->setFlash('error', "Proveedor no Actualizado");
            }
        }
        
        return $this->render('update', [
                    'model'  => $model,
        ]) ;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $model = Proveedor::findOne($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "Proveedor Eliminado");
                
            return $this->redirect(Url::to(['proveedor/index']));
        }else{
            Yii::$app->session->setFlash('error', "Proveedor no Eliminado");
        }
    }
}
