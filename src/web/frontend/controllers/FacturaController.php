<?php

namespace frontend\controllers;

use Yii;
use common\models\Factura;
use common\models\FacturaSearch;
use common\models\ResumenprodSearch;
use common\models\EstatusprovSearch;
use common\models\PagospendSearch;
use common\models\CondicionPago;
use app\models\Marca;
use app\models\EstatusFactura;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use yii\base\DynamicModel;
/**
 * FacturaController implements the CRUD actions for Factura model.
 */
class FacturaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Factura models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacturaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Factura model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {        
        $dataProvider = new ActiveDataProvider([
            'query' => CondicionPago::find()-> where(['id_factura' => $id]),
          ]);
          
        $dataProvider->sort->sortParam = false;
          
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider'=>$dataProvider,
        ]);
    }

    /**
     * Creates a new Factura model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Factura();
        $itemsMarca=ArrayHelper::map(\common\models\Marca::find()->asArray()->all(), 'id_marca', 'descripcion');
        $itemsTemporada=ArrayHelper::map(\common\models\Temporada::find()->asArray()->all(), 'id_temporada', 'descripcion');
        $itemsMes=ArrayHelper::map(\common\models\Mes::find()->asArray()->all(), 'id_mes', 'descripcion');
        $itemsProveedor=ArrayHelper::map(\common\models\Proveedor::find()->asArray()->all(), 'id_proveedor', 'nombre');
        $itemsProducto=ArrayHelper::map(\common\models\TipoProducto::find()->asArray()->all(), 'id_tipo_producto', 'descripcion');
                
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($model->load(Yii::$app->request->post())) {

            $model->attributes=$_POST['Factura'];

            $model->fecha_factura_inicial = date('Y-m-d');
            $model->id_estatus = 1;
            $model->id_usuario =Yii::$app->user->identity->id;

            $model->save();
            
            Yii::$app->session->setFlash('success','Factura registrada satisfactoriamente'); 
            
            return $this->redirect(['index']);
//            return $this->redirect(['view', 'id' => $model->id_factura]);

        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'itemsMarca' => $itemsMarca,
                'itemsTemporada'=>$itemsTemporada,
                'itemsMes'=>$itemsMes,
                'itemsProveedor'=>$itemsProveedor,
                'itemsProducto'=>$itemsProducto,
            ]);
        }

    }
 
    /**
     * Borrar Cuota
     * @param type $idCuota
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionDeletecuota() {
        $idCuota   = Yii::$app->request->get('id') ;
        $condicion = CondicionPago::findOne($idCuota) ;
        if ($condicion->delete()) {
            Yii::$app->session->setFlash('success', "Condición de pago eliminada") ;
        } else {
            Yii::$app->session->setFlash('danger', "Condición de pago no eliminada") ;
        }
        return $this->redirect(Url::to(['factura/update', 'id' => Yii::$app->request->get('id_factura'), 'p' => true])) ;
    }
    
    /**
     * Raíz para actualizar todo referente a la factura
     * @param type $id
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionUpdate($id) {
        $model        = Factura::findOne($id) ;
        $facturaCuota = new CondicionPago ;

        $condicion = CondicionPago::find()->where(['id_factura' => $id])->all() ;

        $activeFactura = "" ;
        if ($condicion) {
            foreach ($condicion as $value) {
                $factura = \app\models\Pago::find()->where(['id_cuota' => $value['id_cuota']])->one() ;
                if ($factura) {
                    $activeFactura = 1 ;
                }
            }
        }
        $activeFactura = ($activeFactura) ? "disabled" : "" ;

        $activeCondicion = Yii::$app->request->get('p') ;
        $activeDate      = Yii::$app->request->get('f') ;

        $montoFactura = (isset($model->monto_factura_final)) ? $model->monto_factura_final : $model->monto_factura_inicial ;

        return $this->render('update', [
                    'model'           => $model,
                    'facturaCuota'    => $facturaCuota,
                    'condicion'       => $condicion,
                    'activeCondicion' => $activeCondicion,
                    'activeDate'      => $activeDate,
                    'activeFactura'   => $activeFactura,
                    'montoFactura'    => $montoFactura,
                    'estatus'         => ArrayHelper::map(EstatusFactura::find()->all(), 'id_estatus', 'descripcion'),
                    'marca'           => ArrayHelper::map(\app\models\Marca::find()->all(), 'id_marca', 'descripcion'),
                    'temporada'       => ArrayHelper::map(\common\models\Temporada::find()->all(), 'id_temporada', 'descripcion'),
                    'tipoproducto'    => ArrayHelper::map(\app\models\TipoProducto::find()->all(), 'id_tipo_producto', 'descripcion'),
                    'mes'             => ArrayHelper::map(\app\models\Mes::find()->all(), 'id_mes', 'descripcion'),
                    'proveedor'       => ArrayHelper::map(\app\models\Proveedor::find()->all(), 'id_proveedor', 'nombre'),
                ]) ;
    }
    
    
    /**
     * Comparación de los porcentajes de la factura con relación a las cuotas
     * @param type $idFactura
     * @param CondicionPago $condicion
     * @return boolean
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    private function comparePorcent($idFactura, $condicion) {
        $condiciones = CondicionPago::find()->where(['id_factura' => $idFactura])->all() ;
        $sumPorc     = 0 ;
        foreach ($condiciones as $value) {
            $sumPorc = $sumPorc + $value->porcentaje ;
        }
        $total = $sumPorc + $condicion->porcentaje ;

        if ($total <= 100) {
            return TRUE ;
        }
        return FALSE ;
    }
    
    /**
     * Registro de cuotas
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionPaycuotas() {
        $post         = Yii::$app->request->post() ;
        $factura      = Factura::findOne($post['CondicionPago']['idfactura']) ;
        $montoFactura = (isset($factura->monto_factura_final)) ? $factura->monto_factura_final : $factura->monto_factura_inicial ;
        $montoCuota   = $post['CondicionPago']['porcentaje'] * $montoFactura / 100 ;

        $fecha     = date($post['CondicionPago']['fecha_pago']) ;
        $fechaSt   = strtotime($fecha . ' +'.(string)$post['CondicionPago']['dias_credito'].' day') ;
        $fechaVenc = date('Y-m-d', $fechaSt) ;

        $condicion                      = new CondicionPago ;
        $condicion->load($post) ;
        $condicion->id_factura          = $post['CondicionPago']['idfactura'] ;
        $condicion->fecha_estimada_pago = $fechaVenc ;
        $condicion->monto_estimado_pago = $montoCuota ;
        $condicion->porcentaje          = str_replace("%", "", $condicion->porcentaje) ;
        $condicion->porcentaje          = (int) $condicion->porcentaje ;
        $condicion->id_estatus_pago     = \common\models\EstatusPago::ESTATUS_PAGO_PENDIENTE ;
        $p                              = false ;
        if ($condicion->validate() && $this->comparePorcent($post['CondicionPago']['idfactura'], $condicion)) {
            $condicion->save() ;
            $p = true ;
            Yii::$app->session->setFlash('success', "Condición de pago guardada") ;
        } else {
            $p = true ;

            Yii::$app->session->setFlash('danger', "Condición de pago no guardada") ;
        }

        return $this->redirect(Url::to(['factura/update', 'id' => $post['CondicionPago']['idfactura'], 'p' => $p])) ;
    }

   /**
     * Actualización de Datos básicos de la factura
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionDatosfactura() {
        if (Yii::$app->request->isPost) {
            $post  = Yii::$app->request->post() ;
            $model = Factura::findOne($post['Factura']['id_factura']) ;

            if ($model->validate() && $model->load($post) ) {

//                $model->monto_factura_final = $post['Factura']['monto_factura_final'];

                if (($post['Factura']['monto_factura_final'])!= NULL){
                    $model->fecha_factura_final = date('Y-m-d');
                } 
//                print_r($model->monto_factura_final );EXIT;
                
                $model->save();

                $this->updateCuota($post['Factura']['id_factura']) ;
                
                Yii::$app->session->setFlash('success', "Datos de factura Actualizada") ;
            } else {
                Yii::$app->session->setFlash('danger', "Datos de factura No Actualizada") ;
            }
        }
        return $this->redirect(Url::to(['factura/update', 'id' => $post['Factura']['id_factura'], 'p' => false])) ;
    }

    /**
     * Actualiza cuota inicial a final 
     * @param type $idfactura
     * @return boolean
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    private function updateCuota($idfactura) {
        $factura = Factura::findOne($idfactura) ;
        $cuotas  = CondicionPago::find()->where(['id_factura' => $idfactura])->all() ;
        $monto   = (isset($factura->monto_factura_final)) ? $factura->monto_factura_final : $factura->monto_factura_inicial ;

        foreach ($cuotas as $value) {
            $cuota                      = CondicionPago::findOne($value['id_cuota']) ;
            $montoCuota                 = $cuota->porcentaje * $monto / 100 ;
            $cuota->monto_estimado_pago = $montoCuota ;
            $cuota->save() ;
        }

        return TRUE ;
    }

    /**
     * Actualización de fechas
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionFechas() {
        $post    = Yii::$app->request->post() ;
        $factura = Factura::findOne($post['Factura']['id_factura']) ;
        if ($factura->load($post) && $factura->save()) {
            Yii::$app->session->setFlash('success', "Fechas Actualizadas") ;
        } else {
            Yii::$app->session->setFlash('danger', "Fechas No Actualizadas") ;
        }
        return $this->redirect(Url::to(['factura/update', 'id' => $post['Factura']['id_factura'], 'f' => TRUE])) ;
    }
    
        
    protected function findModel($id)
    {
        if (($model = Factura::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     /**
     * Reporte Resumen de producción
     * @return type
     * @author Yusmary López <ylopez@grudu.org>
     */
    public function actionReportResumen()
    {
        $searchModel = new ResumenprodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $meses[] = \common\models\Mes::find()->select('descripcion')->all();

        return $this->render('report_resumenprod', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'meses' => $meses,
        ]);
    }
    
    /**
     * Reporte Resumen de estatus proveedor
     * @return type
     * @author Yusmary López <ylopez@grudu.org>
     */
    public function actionReportEstatusprov()
    {
        $searchModel = new EstatusprovSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('report_estatusprov', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Reporte de pagos pendientes
     * @return type
     * @author Yusmary López <ylopez@grudu.org>
     */
    public function actionReportPagospend()
    {
        $searchModel = new PagospendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('report_pagospend', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
     /**
     * Reporte Detallado de producción
     * @return type
     * @author Yusmary López <ylopez@grudu.org>
     */
    public function actionReportDetallado() 
    {
        $model       = new \frontend\models\DetalladoProdForm();
        $marcasData  = Marca::find()->all() ;
        $marcas      = ArrayHelper::map($marcasData, 'id_marca', 'descripcion') ;

        if (Yii::$app->request->get('DetalladoProdForm')) {
            
            if ( $model->load(Yii::$app->request->get()) && $model->validate() ) {
                $column = [
                    [
                        'attribute' => 'dia',
                        'label' => 'Día',
                        'value' => 'dia'
                    ],
                    [
                        'attribute' => 'Enero',
                        'label' => 'Enero',
                        'value' => 'Enero',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Febrero',
                        'label' => 'Febrero',
                        'value' => 'Febrero',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Marzo',
                        'label' => 'Marzo',
                        'value' => 'Marzo',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Abril',
                        'label' => 'Abril',
                        'value' => 'Abril',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Mayo',
                        'label' => 'Mayo',
                        'value' => 'Mayo',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Junio',
                        'label' => 'Junio',
                        'value' => 'Junio',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Julio',
                        'label' => 'Julio',
                        'value' => 'Julio',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Agosto',
                        'label' => 'Agosto',
                        'value' => 'Agosto',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                   [
                        'attribute' => 'Septiembre',
                        'label' => 'Septiembre',
                        'value' => 'Septiembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Octubre',
                        'label' => 'Octubre',
                        'value' => 'Octubre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Noviembre',
                        'label' => 'Noviembre',
                        'value' => 'Noviembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Diciembre',
                        'label' => 'Diciembre',
                        'value' => 'Diciembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                ];
                
                $dataProvider = new \yii\data\ActiveDataProvider([

                    'query'=>CondicionPago::find()
                        ->select('dayofmonth(condicion_pago.fecha_estimada_pago) as dia,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=1 then condicion_pago.monto_estimado_pago end )AS Enero,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=2 then condicion_pago.monto_estimado_pago end )AS Febrero,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=3 then condicion_pago.monto_estimado_pago end )AS Marzo,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=4 then condicion_pago.monto_estimado_pago end )AS Abril,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=5 then condicion_pago.monto_estimado_pago end )AS Mayo,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=6 then condicion_pago.monto_estimado_pago end )AS Junio,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=7 then condicion_pago.monto_estimado_pago end )AS Julio,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=8 then condicion_pago.monto_estimado_pago end )AS Agosto,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=9 then condicion_pago.monto_estimado_pago end )AS Septiembre,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=10 then condicion_pago.monto_estimado_pago end )AS Octubre,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=11 then condicion_pago.monto_estimado_pago end )AS Noviembre,
                       sum(case when month(condicion_pago.fecha_estimada_pago)=12 then condicion_pago.monto_estimado_pago end )AS Diciembre')
                       ->innerJoin('factura','factura.id_factura=condicion_pago.id_factura')
                       ->innerJoin('marca','factura.id_marca=marca.id_marca')
                       ->where([
                              'factura.id_marca' => $model->marca,
                              'year(condicion_pago.fecha_estimada_pago)' => $model->year,
                               ])
                       ->groupBy('dayofmonth(condicion_pago.fecha_estimada_pago)')
                ]) ;
                

                return $this->render('report_detalladoprod', [
                            'marcas'  => $marcas,
                            'model'   => $model,
                            'dataProvider'   => $dataProvider,
                            'column'  => $column
                ]) ;
            }
            
        }
        
        return $this->render('report_detalladoprod', [
                    'marcas'  => $marcas,
                    'model'   => $model
        ]) ;
    }
}
