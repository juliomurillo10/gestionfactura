<?php
namespace frontend\controllers;

use Yii;
use app\models\Factura;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use app\models\CondicionPago;

/**
 * Pagos controller
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class PagosController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        
    }
    
    /**
     * Agregar pagos a la factura
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionAdd() {
        $dataFac = Factura::find()->all() ;
        $data    = ArrayHelper::map($dataFac, 'id_factura', 'nro_factura') ;

        $factura = null ;
        $cuotas  = null ;
        $pago    = null ;
        $monto   = null ;
        
        if (Yii::$app->request->isPost) {
            $idFactura = Yii::$app->request->post('n_factura') ;
            $factura   = Factura::findOne($idFactura) ;
            $cuotas    = CondicionPago::find()->where(['id_factura' => $idFactura])->all() ;

            if ($cuotas) {

                foreach ($cuotas as $value) {

                    $pagoModel = \app\models\Pago::find()->where(['id_cuota' => $value['id_cuota']])->all() ;

                    if ($pagoModel) {
                        $montos = 0 ;
                        
                        foreach ($pagoModel as $valor) {
                            $montos                   = $montos + $valor['monto_pago'] ;
                            $pago[$value['id_cuota']] = new \app\models\Pago ;
                        }
                        
                        $monto[$value['id_cuota']]['abonado']   = $montos ;
                        $pendiente                              = $value['monto_estimado_pago'] - $montos ;
                        $monto[$value['id_cuota']]['pendiente'] = $pendiente ;
                        
                    } else {
                        
                        $pago[$value['id_cuota']]               = new \app\models\Pago ;
                        $monto[$value['id_cuota']]['abonado']   = 0 ;
                        $monto[$value['id_cuota']]['pendiente'] = $value['monto_estimado_pago'] ;
                    }
                }
            }
        }
        return $this->render('add', [
                    'data'    => $data,
                    'factura' => $factura,
                    'cuotas'  => $cuotas,
                    'pago'    => $pago,
                    'monto'   => $monto,
        ]) ;
    }

    /**
     * Método Ajax para registrar el pago definido por la cuota
     * @author Julio Murillo <jmurillo@grudu.org>
     * @return boolean
     */
    public function actionPay() {
        $id_cuota             = Yii::$app->request->post('id_cuota') ;
        $monto                = Yii::$app->request->post('monto') ;
        $fecha                = Yii::$app->request->post('fecha') ;
        $pago                 = new \app\models\Pago() ;
        $pago->id_cuota       = $id_cuota ;
        $pago->monto_pago     = $monto ;
        $pago->fecha_pago     = $fecha ;
        $pago->fecha_creacion = date('Y-m-d') ;
        $pago->id_usuario     = \Yii::$app->user->id ;

        $pagoModel = \app\models\Pago::find()->where(['id_cuota' => $id_cuota])->all() ;
        $montos    = 0 ;
        
        if ($pagoModel) {
            
            foreach ($pagoModel as $valor) {
                $montos = $montos + $valor['monto_pago'] ;
            }
        }
        
        $total = $montos + $monto ;
        $cuota = CondicionPago::findOne($id_cuota) ;
        
        if ($total <= $cuota->monto_estimado_pago) {
            $pago->save() ;
            $this->statusCuota($total, $cuota) ;
            Yii::$app->session->setFlash('success', "Pago guardado") ;
            return true ;
            
        } else {
            Yii::$app->session->setFlash('danger', "Pago no guardado, monto mayor al monto de la cuota") ;
            return false ;
        }
    }

    /**
     * Actualizar status cuota
     * @author Julio Murillo <jmurillo@grudu.org>
     * @param type $total
     * @param CondicionPago $cuota
     */
    public function statusCuota($total, $cuota) {
        
        if ($total == $cuota->monto_estimado_pago) {
            
            $cuota->id_estatus_pago = \common\models\EstatusPago::ESTATUS_PAGO_CANCELADA ;
            $cuota->save() ;
            $this->statusFactura($cuota->id_factura) ;
        } else {
            $cuota->id_estatus_pago = \common\models\EstatusPago::ESTATUS_PAGO_PARCIALMENTE_CANCELADA ;
            $factura                = Factura::findOne($cuota->id_factura) ;
            $factura->id_estatus    = \app\models\EstatusFactura::ESTATUS_FACTURA_PARCIALMENTE_CANCELADA ;
            $factura->save() ;
            $cuota->save() ;
        }
        
    }

    /**
     * Actualizar estatus de factura
     * @param type $idFactura
     * @return boolean
     */
    public function statusFactura($idFactura) {

        $factura   = Factura::findOne($idFactura) ;
        $cuotas    = CondicionPago::find()->where(['id_factura' => $idFactura])->all() ;
        $cancelada = 1 ;
        
        foreach ($cuotas as $value) {
            
            if ($value['id_estatus_pago'] != \common\models\EstatusPago::ESTATUS_PAGO_CANCELADA) {
                $cancelada = 0 ;
            } 
        }
        
        if ($cancelada == 1) {
            $factura->id_estatus = \app\models\EstatusFactura::ESTATUS_FACTURA_CANCELADA ;
            
        } else {
            $factura->id_estatus = \app\models\EstatusFactura::ESTATUS_FACTURA_PARCIALMENTE_CANCELADA ;
        }

        if ($factura->save()) {
            return true ;
        }
        
        return false ;
    }

}