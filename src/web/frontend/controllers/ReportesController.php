<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Marca;
use app\models\EstatusFactura;
use app\models\Factura;

/**
 * ReportesController implements the CRUD actions for Reportes model.
 * @author Julio Murillo <jmurillo@grudu.org>
 */
class ReportesController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Factura por estatus
     * @return type
     * @author Julio Murillo <jmurillo@grudu.org>
     */
    public function actionFacturaStatus() {
        $model       = new \frontend\models\FacturaStatusForm();        
        $marcasData  = Marca::find()->all() ;
        $marcas      = ArrayHelper::map($marcasData, 'id_marca', 'descripcion') ;
        $estatusData = EstatusFactura::find()->all() ;
        $estatus     = ArrayHelper::map($estatusData, 'id_estatus', 'descripcion') ;

        if (Yii::$app->request->get('FacturaStatusForm')) {
            
            if ( $model->load(Yii::$app->request->get()) && $model->validate() ) {
                $column = [
                    'idMarca.descripcion',
                    'idTemporada.descripcion',
                    'idMes.descripcion',
                    'ano',
                    'idProveedor.nombre',
                    'idTipoProducto.descripcion',
                    'nro_factura',
                    [
                    'attribute' =>'monto_factura_inicial',
                     'label' =>'Monto factura inicial',
                    'format' => 'currency',
                     ],
                    'unidades_factura_inicial',
                    [
                    'attribute' =>'monto_factura_final',
                     'label' =>'Monto factura final',
                    'format' => 'currency',
                     ],
                    'unidades_factura_final',
                    'fecha_proforma',
                    'fecha_produccion',
                    'fecha_despacho',
                    'fecha_llegada',
                    'fecha_almacen',
                    'idEstatus.descripcion'
                ];
                
                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\Factura::find()
                        ->where(['id_marca'=> $model->marca])
                        ->andWhere(['id_estatus'=>$model->estatus])
                        ->andWhere(['between', 'fecha_proforma',$model->desde,$model->hasta]),
//                        ->andWhere(['between', 'fecha_factura_inicial',$model->desde,$model->hasta]),
                ]) ;
                
                return $this->render('facturastatus', [
                            'marcas'  => $marcas,
                            'estatus' => $estatus,
                            'model'   => $model,
                            'dataProvider'   => $dataProvider,
                            'column'  => $column
                ]) ;
            }
            
        }
        
        
        return $this->render('facturastatus', [
                    'marcas'  => $marcas,
                    'estatus' => $estatus,
                    'model'   => $model
        ]) ;
    }

    public function actionExcel() {
        $datos = \Yii::$app->request->post('datosFormulario')[0];
        $factura = Factura::find()->where(['id_marca'=>$datos['marca']])
                                ->andWhere(['id_estatus'=>$datos['estatus']])
                                ->all();
        
        print_r($datos);exit;
        
    }
}