<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\date\DatePicker;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-2 col-xs-12">
            <div class="form-group">
                <?php $form1 = ActiveForm::begin([
                 'action' => ['factura/paycuotas'],
            ]) ; ?>
                <label for="total-fact">Total Factura:</label>
                <?=
 kartik\money\MaskMoney::widget([
                        'name' => 'total-fact',
                        'value' => $montoFactura,
                        'disabled' => true,
                        'pluginOptions' => [
                            'prefix' => '$ ',
                            'allowNegative' => false
                        ]
                    ]);
                ?>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-2 col-lg-offset-1">
            <label>Porcentaje de la cuota</label>
        </div>
        <div class="col-lg-2">
            <label>Monto cuota</label>
        </div>
        <div class="col-lg-2">
            <label>Fecha de pago</label>
        </div>
        <div class="col-lg-2">
            <label>Días de Crédito</label>
        </div>
        <div class="col-lg-2">
            <label>Fecha de Vencimiento</label>
        </div>
        <div class="col-lg-1">
        </div>
    </div>
    <div class="amount">
        <div class="row">
            
            <div class="col-lg-2 col-lg-offset-1">
                <?= $form1->field($facturaCuota, 'porcentaje')->widget(MaskedInput::className(),[
                    'mask' => "99%",
                ])->label(false) ?>

            </div>
            <div class="col-lg-2">
                <?= $form1->field($facturaCuota, 'monto_estimado_pago')->widget(kartik\money\MaskMoney::className(),[
                    'disabled' => true,
                    'pluginOptions' => [
                           'prefix' => '$ ',
                           'allowNegative' => false
                       ]
                    ])->label(false) ?>
            </div>
            <div class="col-lg-2">
                <?= DatePicker::widget([
                    'model' => $facturaCuota, 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'attribute' => 'fecha_pago',
                    'options' => ['placeholder' => 'Ingresar Fecha de Producción ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]); ?>
            </div>
            <div class="col-lg-2">
                <?= $form1->field($facturaCuota, 'dias_credito')->textInput()->label(false) ?>
                <?= $form1->field($facturaCuota, 'idfactura')->hiddenInput(['value'=>Yii::$app->request->get('id')])->label(false) ?>
            </div>
            <div class="col-lg-2">
                <?= $form1->field($facturaCuota, 'fecha_estimada_pago')->textInput(['disabled'=>'disabled'])->label(false) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::submitButton('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>',['class' => 'btn btn-info'])?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
        <br>
    </div>
    <div class="duplicate">
        <?php
        if ($condicion) : 
            foreach ($condicion as $value) :
        ?>
             <div class="row">
                <div class="col-lg-2 col-lg-offset-1">
                    <input name="porce-cuota" type="text" id="porce-cuota" placeholder="" value="<?= $value->porcentaje ?>" class="form-control" disabled="disabled">
                </div>
                <div class="col-lg-2">
                    <input name="mont-cuota" type="text" id="mont-cuota" placeholder="" value="<?= $value->monto_estimado_pago ?>" class="form-control" disabled="disabled">
                </div>
                <div class="col-lg-2">
                    <input name="dias-cuota" type="text" id="dias-cuota" placeholder="" value="<?= $value->fecha_pago ?>" class="form-control" disabled="disabled">
                </div>
                <div class="col-lg-2">
                    <input name="dias-cuota" type="text" id="dias-cuota" placeholder="" value="<?= $value->dias_credito ?>" class="form-control" disabled="disabled">
                </div>
                <div class="col-lg-2">
                    <input name="fech-venc" type="text" id="fech-venc" placeholder="" value="<?= $value->fecha_estimada_pago ?>" class="form-control" disabled="disabled">
                </div>
                <div class="col-lg-1">
                    <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', ['deletecuota','id_factura' => $value->id_factura ,'id' => $value->id_cuota], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        <br>
        <?php
            endforeach;
        endif;
        ?>
    </div>
   
    
<?php
$script = <<< JS
    $( "#plus" ).click(function() {
        var porc = $( "#porce-cuota" ).val();
        var mont = $( "#mont-cuota" ).val();
        var dias = $( "#dias-cuota" ).val();
        var fech = $( "#fech-venc" ).val();
        
        console.log(porc);
        
        var valor = '<div class="row">'+
                '<div class="col-lg-2 col-lg-offset-2">'+
                    '<input name="porce-cuota" type="text" id="porce-cuota" placeholder="" value="'+porc+'" class="form-control" disabled>'+
                '</div>'+
                '<div class="col-lg-2">'+
                    '<input name="mont-cuota" type="text" id="mont-cuota" placeholder="" value="'+mont+'" class="form-control" disabled>'+
                '</div>'+
                '<div class="col-lg-2">'+
                    '<input name="dias-cuota" type="text" id="dias-cuota" placeholder="" value="'+dias+'" class="form-control" disabled>'+
                '</div>'+
                '<div class="col-lg-2">'+
                    '<input name="fech-venc" type="text" id="fech-venc" placeholder="" value="'+fech+'" class="form-control" disabled>'+
                '</div>'+
                '<div class="col-lg-2">'+
                    '<button type="button" class="btn btn-danger" id="plus">-</button>'+
                '</div>'+
            '</div>'+
            '<br>';
        
        limpiar();
        
        $(".duplicate").append(valor);
    });
    
    function limpiar (){
        $( "#porce-cuota" ).val('');
        $( "#mont-cuota" ).val('');
        $( "#dias-cuota" ).val('');
        $( "#fech-venc" ).val('');
    }
        
    $('#condicionpago-porcentaje').on('keyup', function(e){
        var value = $(this).val();
        var monto = parseInt($montoFactura);
        var value = parseInt(value);
        var result = $montoFactura * value /100;
        if (isNaN(result)) {
            $('#condicionpago-monto_estimado_pago-disp').val(0);
            $('#condicionpago-monto_estimado_pago').val(0);
        }else{
            $('#condicionpago-monto_estimado_pago-disp').val(result);
            $('#condicionpago-monto_estimado_pago').val(result);
        }
        console.log(result);
        e.stopPropagation();
        e.preventDefault();
    });
    
    $('#condicionpago-fecha_pago').on('change', function(e){
        var credito = $('#condicionpago-dias_credito').val();
        if (credito != "") {
            var value = $(this).val();
            var date = new Date(value);
            var newDate = date.setDate(date.getDate()+parseInt(credito)+1);
            var dates = new Date(newDate);
            $('#condicionpago-fecha_estimada_pago').val(dates.getFullYear() + '-' + (dates.getMonth()+1) + '-' + dates.getDate() );
        }
    });
    
    $('#condicionpago-dias_credito').on('change', function(e){
        var fecha = $('#condicionpago-fecha_pago').val();
        if ( fecha != "" ) {
            var value = $('#condicionpago-fecha_pago').val();
            var date = new Date(value);
            var newDate = date.setDate(date.getDate()+parseInt($(this).val())+1);
            var dates = new Date(newDate);
            $('#condicionpago-fecha_estimada_pago').val(dates.getFullYear() + '-' + (dates.getMonth()+1) + '-' + dates.getDate() );
        }
        e.stopPropagation();
        e.preventDefault();
    });
JS;
$this->registerJs($script);
?>