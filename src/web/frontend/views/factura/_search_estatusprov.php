<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\EstatusprovSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="factura-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-estatusprov'],
        'method' => 'get',
        'options'=>['id'=>'resumenprod-form'],
    ]);
    ?>
    
    <?=Html::activeDropDownList($model, 'idProveedor', ArrayHelper::map(common\models\Proveedor::find()->asArray()->all(), 'nombre', 'nombre'),['class'=>'form-control','prompt' => 'Seleccione proveedor'])?>
    <br>
     <?php echo '<label class="control-label">Seleccione el rango de fechas</label>';
echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'start_date',
    'attribute2' => 'end_date',
    'options' => ['placeholder' => 'Fecha Inicio'],
    'options2' => ['placeholder' => 'Fecha Fin'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
    ]
]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
