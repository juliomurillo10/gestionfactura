<?php

?>
<!DOCTYPE html>
<html>
<head>
    <title>Print Resumen Producción</title>
    <style>
        .page
        {           
            padding:2cm;
        }
        table
        {
            border-spacing:0;
            border-collapse: collapse; 
            width:100%;
        }

        table td, table th
        {
            border: 1px solid #ccc;
        }
        
        table th
        {
            background-color:#b6b6b6;
        }
    </style>
</head>
<body>	
    <div class="page">	
        <h1>Resumen Producción</h1>
        <table border="0">
        <tr>
        <th>Marcas</th>
        <?php
         foreach($itemsMes as $mes){ 
        ?>
                <!--<th>No</th>-->
            <th><?php echo $mes->descripcion ;?></th>
        <?php
        }
        ?>
        </tr>
        <?php
            foreach($itemsMarca as $marca){
            ?>
        <tr>
                <td><?php echo $marca->descripcion; ?></td>
                    <?php
                       foreach($itemsMes as $mes){ 
                           $acumulado= 0;
                           //        $no = 1;
                           foreach($dataProvider->getModels() as $factura){ 

                               if(($factura->id_mes == $mes->id_mes) && ($factura->id_marca == $marca->id_marca)){ 
                                   if($factura->monto_factura_final!==null){ 
                                       $acumulado= $acumulado + $factura->monto_factura_final;
                                   } else {
                                       $acumulado= $acumulado + $factura->monto_factura_inicial;
                                   }
                    ?>
                                       <!--<td><? = $no++ ?></td>-->
                                       <td><?php echo $acumulado;?></td>
<!--                    <td>prueba</td> 
                    <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> <td>prueba</td> -->
               </tr>
            <?php
                             }
                           }
                       }
            }
            ?>
        </table>
    </div>   
</body>
</html>
