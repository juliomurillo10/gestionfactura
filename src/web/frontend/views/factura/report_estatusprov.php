<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Alert;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EstatusprovSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estatus por Proveedor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="factura-index">

    <!--<h1><? = Html::encode($this->title) ?></h1>-->
    <?php 
      echo $this->render('_search_estatusprov', ['model' => $searchModel]);
    ?>
    <p>
        <?= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-refresh"></span>'), Url::toRoute(['factura/report-estatusprov']), ['title' => 'Limpiar Búsqueda','class' => 'btn btn-success btn-factura']) ?>
    </p>  
    <?php
                $gridColumns = [
                    [
                        'attribute' => 'idProveedor.nombre',
                        'label' => 'Proveedor',
                        'value' => 'idProveedor.nombre'
                    ],
                    [
                        'attribute' => 'idTipoProducto.descripcion',
                        'label' => 'Tipo de Producto',
                        'value' => 'idTipoProducto.descripcion'
                    ],
                    [
                        'attribute' => 'ano',
                        'label' => 'Año',
                        'value' => 'ano'
                    ],
                    [
                        'attribute' => 'facturado',
                        'label' => 'Total Facturado',
                        'value' => 'facturado',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'abonado',
                        'label' => 'Total Abonado',
                        'value' => 'abonado',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                    'class' => '\kartik\grid\FormulaColumn',
                    'label'=>'Total Pendiente',
                    'pageSummary' => true,
                    'format' => 'currency',
                    'value' => function ($model, $key, $index, $widget) {
                        $p = compact('model', 'key', 'index');
                        // Write your formula below
                        return $widget->col(3, $p) - $widget->col(4, $p);
                    }
                    ],
    ];
    ?>
    <div class="container-grid">
       <?php  echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_HTML => false,
//                ExportMenu::FORMAT_TEXT => false,
//                ExportMenu::FORMAT_EXCEL_X => false,
            ],
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Exportar todo',
                'class' => 'btn btn-default'
            ],
        ])?>
            
        <?php Pjax::begin(); ?>    

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'responsive'=> false,
                'showPageSummary' => true ,
                'pageSummaryRowOptions' => ['class' => 'kv-page-summary danger'],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-','thousandSeparator' => '.','decimalSeparator' => ',','currencyCode' => '$'],
                'columns' => $gridColumns,
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <?php Alert::widget();?>
</div>
