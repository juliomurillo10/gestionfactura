<?php

use yii\helpers\Url ;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title= 'Editar Factura '.$model->nro_factura ;
$this->params['breadcrumbs'][] = ['label' => 'Facturas', 'url' => Url::to(['factura/index'])] ;
$this->params['breadcrumbs'][] = $this->title ;

?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active" ><a href="#fa-icons" data-toggle="tab">Datos Factura</a></li>
                    <li class=""><a href="#glyphicons" data-toggle="tab">Fechas</a></li>
                    <li class="" ><a href="#condiciones" data-toggle="tab">Condiciones de Pago</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="fa-icons">
                        <?= $this->render('datosfactura',[
                            'model' => $model,
                            'estatus' => $estatus,
                            'marca' => $marca,
                            'temporada' => $temporada,
                            'tipoproducto' => $tipoproducto,
                            'mes' => $mes,
                            'proveedor' => $proveedor,
                            'activeFactura' => $activeFactura,
                        ]) ?>
                    </div>

                    <div class="tab-pane" id="glyphicons">
                        <?= $this->render('fechas',[
                            'model' => $model,
                            'estatus' => $estatus,
                        ]) ?>

                    </div>
                    <div class="tab-pane" id="condiciones">
                        <?= $this->render('condicionespago',[
                            'model' => $model,
                            'estatus' => $estatus,
                            'facturaCuota' => $facturaCuota,
                            'condicion' => $condicion,
                            'montoFactura' => $montoFactura,
                        ]) ?>

                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->