<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Factura */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Resumen de Producción';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="factura-search">

    <?php $form = ActiveForm::begin(['id'=>'reporte','method' => 'get'],['class' => 'form-horizontal']); ?>
    
        <?= $form->field($model, 'id_marca')->dropDownList($itemsMarca,['prompt'=>'Seleccione']) ?>

        <?= $form->field($model, 'id_tipo_producto')->dropDownList($itemsProducto,['prompt'=>'Seleccione']) ?>

    <?php echo '<label class="control-label">Seleccione el rango de fechas</label>';
        echo DatePicker::widget([
        //    'model' => $model,
            'name' => 'from_date',
            'value' => date('Y-m-d'),
            'type' => DatePicker::TYPE_RANGE,
            'name2' => 'to_date',
            'value2' => date('Y-m-d'),
            'options' => ['placeholder' => 'Inicio'],
            'options2' => ['placeholder' => 'Fin'],
        //    'form' => $form,
            'pluginOptions' => [
                 'format' => 'yyyy-mm-dd',
                 'autoclose' => true,
                ]
        ]);
    ?>
       <div class="division-header"></div><br><br>
        
       <div class="modal-footer">
           <!-- EXAMPLE BUTTON EXPORT PHPEXCEL -->
            <?= Html::a('Export Excel', ['export-excel'], ['class'=>'btn btn-info']); ?>  

            <!-- EXAMPLE BUTTON EXPORT OPENTBS -->
            <!--<? = Html::a('Export Word', ['export-word'], ['class'=>'btn btn-warning']); ?>-->  
            <!--<? = Html::a('Export Excel', ['export-excel2'], ['class'=>'btn btn-info']); ?>-->  

            <!-- EXAMPLE BUTTON EXPORT MPDF -->
            <?= Html::a('Export PDF', ['export-pdf'], ['class'=>'btn btn-success']); ?>  

            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>-->
            <!--<? = Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>-->
       </div>

    <?php ActiveForm::end(); ?>

</div>

