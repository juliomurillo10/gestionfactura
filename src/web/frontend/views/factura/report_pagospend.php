<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Alert;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EstatusprovSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pagos Pendientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="factura-index">

    <?php 
      echo $this->render('_search_pagospend', ['model' => $searchModel]);
    ?>
    <p>
        <?= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-refresh"></span>'), Url::toRoute(['factura/report-pagospend']), ['title' => 'Limpiar Búsqueda','class' => 'btn btn-success btn-factura']) ?>
    </p>  
    <?php
                $gridColumns = [
                    [
                        'attribute' => 'marca',
                        'label' => 'Marcas',
                        'value' => 'marca'
                    ],
                    [
                        'attribute' => 'producto',
                        'label' => 'Tipo de Producto',
                        'value' => 'producto'
                    ],
                   [
                        'attribute' => 'Enero',
                        'label' => 'Enero',
                        'value' => 'Enero',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Febrero',
                        'label' => 'Febrero',
                        'value' => 'Febrero',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Marzo',
                        'label' => 'Marzo',
                        'value' => 'Marzo',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Abril',
                        'label' => 'Abril',
                        'value' => 'Abril',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Mayo',
                        'label' => 'Mayo',
                        'value' => 'Mayo',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Junio',
                        'label' => 'Junio',
                        'value' => 'Junio',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Julio',
                        'label' => 'Julio',
                        'value' => 'Julio',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Agosto',
                        'label' => 'Agosto',
                        'value' => 'Agosto',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                   [
                        'attribute' => 'Septiembre',
                        'label' => 'Septiembre',
                        'value' => 'Septiembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Octubre',
                        'label' => 'Octubre',
                        'value' => 'Octubre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Noviembre',
                        'label' => 'Noviembre',
                        'value' => 'Noviembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
                    [
                        'attribute' => 'Diciembre',
                        'label' => 'Diciembre',
                        'value' => 'Diciembre',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],  
    ];
    ?>
    <div class="container-grid">
       <?php  echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Exportar todo',
                'class' => 'btn btn-default'
            ],
        ])?>
            
        <?php Pjax::begin(); ?>    

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'responsive'=> false,
                'showPageSummary' => true ,
                'pageSummaryRowOptions' => ['class' => 'kv-page-summary danger'],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-','thousandSeparator' => '.','decimalSeparator' => ',','currencyCode' => '$'],
                'columns' => $gridColumns,
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <?php Alert::widget();?>
</div>
