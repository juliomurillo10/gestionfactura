<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Alert;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FacturaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumen de Producción';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="factura-index">

    <!--<h1><? = Html::encode($this->title) ?></h1>-->
    <?php 
      echo $this->render('_search_resumenprod', ['model' => $searchModel]);
    ?>
    <p>
        <?= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-refresh"></span>'), Url::toRoute(['factura/report-resumen']), ['title' => 'Limpiar Búsqueda','class' => 'btn btn-success btn-factura']) ?>
    </p>  
    <?php
                $gridColumns = [
                    [
                        'attribute' => 'idMarca.descripcion',
                        'label' => 'Marcas',
                        'value' => 'idMarca.descripcion'
                    ],
                    [
                        'attribute' => 'idTipoProducto.descripcion',
                        'label' => 'Tipo de Producto',
                        'value' => 'idTipoProducto.descripcion'
                    ],
                    [
                        'attribute' => 'idMes.descripcion',
                        'label' => 'Mes',
                        'value' => 'idMes.descripcion'
                    ],
                    [
                        'attribute' => 'ano',
                        'label' => 'Año',
                        'value' => 'ano'
                    ],
                    [
                        'attribute' => 'acumulado',
                        'label' => 'Valor Total de Producción',
                        'value' => 'acumulado',
                        'pageSummary' => true,
                        'format' => 'currency',
                    ],
    ];
    ?>
    <div class="container-grid">
       <?php  echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_HTML => false,
//                ExportMenu::FORMAT_TEXT => false,
//                ExportMenu::FORMAT_EXCEL_X => false,
            ],
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Exportar todo',
                'class' => 'btn btn-default'
            ],
        ])?>
            
        <?php Pjax::begin(); ?>    

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'responsive'=> false,
                'showPageSummary' => true ,
                'pageSummaryRowOptions' => ['class' => 'kv-page-summary danger'],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-','thousandSeparator' => '.','decimalSeparator' => ',','currencyCode' => '$'],
                'columns' => $gridColumns,
//              'beforeHeader'=>[
//        [
//            'columns'=> $headerColumns,
//            'options'=>['class'=>'skip-export'] // remove this row from export
//        ]
//    ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <?php Alert::widget();?>
</div>
