<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <?php $form = ActiveForm::begin([
        'action' => ['factura/fechas'],
    ]) ; ?>
    
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_factura')->hiddenInput()->label(false) ?>
            <?php
                // Usage with model (with no default initial value)
                echo '<label class="control-label">Fecha Producción</label>';
                echo DatePicker::widget([
                    'model' => $model, 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'attribute' => 'fecha_produccion',
                    'options' => ['placeholder' => 'Ingresar Fecha de Producción ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            
            
            <br>
            <?php
                // Usage with model (with no default initial value)
                echo '<label class="control-label">Fecha Despacho</label>';
                echo DatePicker::widget([
                    'model' => $model, 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'attribute' => 'fecha_despacho',
                    'options' => ['placeholder' => 'Ingresar Fecha de Despacho ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            <br>
            <?php
                // Usage with model (with no default initial value)
                echo '<label class="control-label">Fecha Llegada</label>';
                echo DatePicker::widget([
                    'model' => $model, 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'attribute' => 'fecha_llegada',
                    'options' => ['placeholder' => 'Ingresar Fecha de Llegada ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            <br>
            <?php
                // Usage with model (with no default initial value)
                echo '<label class="control-label">Fecha Almacen</label>';
                echo DatePicker::widget([
                    'model' => $model, 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'attribute' => 'fecha_almacen',
                    'options' => ['placeholder' => 'Ingresar Fecha de Almacen ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-3 col-lg-offset-9">
            <?= Html::a("Atrás", Url::to(['factura/index']) ,['class' => 'btn btn-default btn-flat btn-fact'])?>
            <?= Html::submitButton("Guardar",['class' => 'btn btn-info btn-flat btn-fact'])?>
        </div>
        
    </div>
    
    <?php ActiveForm::end() ?>