<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($activeFactura);exit;
?>
    <?php $form = ActiveForm::begin([
        'action' => ['factura/datosfactura'],
    ]) ; ?>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-8">
            <?= $form->field($model, 'nro_factura')->textInput() ?>
            <?= $form->field($model, 'id_factura')->hiddenInput()->label(false) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_estatus')->dropDownList($estatus,['disabled'=>'disabled'])?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_marca')->dropDownList($marca) ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_temporada')->dropDownList($temporada) ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_tipo_producto')->dropDownList($tipoproducto) ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_mes')->dropDownList($mes) ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'ano')->textInput() ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?= $form->field($model, 'id_proveedor')->dropDownList($tipoproducto) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2 col-md-offset-3">
            <?= $form->field($model, 'monto_factura_inicial')->textInput(['disabled'=>'disabled']) ?>
            <?= $form->field($model, 'monto_factura_final')->textInput([$activeFactura => $activeFactura]) ?>
        </div>
        <div class="col-md-2 col-md-offset-2">
            <?= $form->field($model, 'unidades_factura_inicial')->textInput(['disabled'=>'disabled']) ?>
            <?= $form->field($model, 'unidades_factura_final')->textInput([$activeFactura => $activeFactura]) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-3 col-lg-offset-9">
            <?= Html::a("Atrás", Url::to(['factura/index']) ,['class' => 'btn btn-default btn-flat btn-fact'])?>
            <?= Html::submitButton("Guardar",['class' => 'btn btn-info btn-flat btn-fact'])?>
        </div>
        
    </div>
    
    <?php ActiveForm::end() ?>