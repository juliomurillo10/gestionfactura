<?php

use yii\helpers\Url ;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title= 'Detallado Producción';
$this->params['breadcrumbs'][] = $this->title ;

?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                
                                <?php $form = ActiveForm::begin([
                                    'method'=>'get'
                                ]); ?>
                            </div>
                            <div class="col-lg-2 text-center">
                                <?= $form->field($model, 'marca')->dropDownList($marcas,['prompt'=>'Seleccione']) ?>
                            </div>
                            <div class="col-lg-2 text-center">
                                <?= $form->field($model, 'year')->dropDownList($model-> getYearsList(),['prompt'=>'Seleccione'])->label('Año') ?> 
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="text-center">
                                <?=
                                    Html::submitButton('Buscar',['class'=>'btn btn-primary','id'=>'excel'])
                                ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                        
                        <?php if( isset($dataProvider)): ?>
                        <hr>
                        <?= ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $column,
                            'exportConfig' => [
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false,
                             ],
                            'fontAwesome' => true,
                            'dropdownOptions' => [
                                'label' => 'Exportar todo',
                                'class' => 'btn btn-default'
                            ],
                            'options' => ['id'=>'expMenu1'], // optional to set but must be unique
                            'target' => ExportMenu::TARGET_POPUP
                        ])?>
                        <?=
                            GridView::widget([
                                'dataProvider'=> $dataProvider,
                                'columns' => $column,
                                'responsive'=>true,
                                'hover'=>true,
                                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-','thousandSeparator' => '.','decimalSeparator' => ',','currencyCode' => '$'],
                            ]);
                        ?>
                        <?php endif ; ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
