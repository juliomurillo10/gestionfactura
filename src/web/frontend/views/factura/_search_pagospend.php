<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\PagospendSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="factura-search">
<div class="row">
   <div class="col-lg-2 text-center">
    <?php $form = ActiveForm::begin([
        'action' => ['report-pagospend'],
        'method' => 'get',
        'options'=>['id'=>'pagospend-form'],
    ]);
    ?>
    </div>
    <div class="col-lg-2 text-center">
    <?=Html::activeDropDownList($model, 'marca', ArrayHelper::map(common\models\Marca::find()->asArray()->all(), 'descripcion', 'descripcion'),['class'=>'form-control','prompt' => 'Seleccione Marca'])?>
    <br>
    </div>
    <div class="col-lg-2 text-center">
    <?= Html::activeDropDownList($model, 'producto', ArrayHelper::map(common\models\TipoProducto::find()->asArray()->all(), 'descripcion', 'descripcion'),['class'=>'form-control','prompt' => 'Seleccione Tipo de Producto'])?>
    <br>
    </div>
    <div class="col-lg-2 text-center">
    <?= $form->field($model, 'year')->dropDownList($model-> getYearsList(),['prompt'=>'Seleccione el año'])->label(false) ?> 
    </div>
  </div>
    <hr>
    <div class="row">
        <div class="text-center">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
