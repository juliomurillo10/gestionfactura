<?php
use common\components\GDhelper;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2dumit-160x160.png" class="img-circle" alt="User Image"/>
                 <!--<img src="<? = Yii::getAlias('@web/img/img-dumit/white/user2dumit-160x160.png') ?>" class="img-circle" alt="User Image"/>-->
            </div>
            <div class="pull-left info">
                <p><?php  echo('<b>'. GDhelper::getName().'</b><br>');?></p>
               
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Sistema Gestión de facturas y pagos', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/gii']],
                    ['label' => 'Factura', 'icon' => 'file', 'url' => ['/factura/index']],
                    ['label' => 'Pago', 'icon' => 'mouse-pointer', 'url' => ['/pago/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Reportes',
                        'icon' => 'file-text',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Producción',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Resumen', 'icon' => 'circle-o', 'url' => ['/factura/report-resumen'],],
                                    ['label' => 'Detallado', 'icon' => 'circle-o', 'url' =>[ '/factura/report-detallado'],],
                                ],
                            ],
                            ['label' => 'Estatus Factura', 'icon' => 'circle-o', 'url' => ['/reportes/factura-status'],],
                            ['label' => 'Estatus Proveedor', 'icon' => 'circle-o', 'url' => ['/factura/report-estatusprov'],],
                            ['label' => 'Pagos Pendientes', 'icon' => 'circle-o', 'url' => ['/factura/report-pagospend'],],
                        ],
                    ],
                    [
                        'label' => 'Carga Inicial',
                        'icon' => 'file-text',
                        'url' => '#',
                        'items' => [
                                ['label' => 'Estatus Factura', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/estatus-factura']),],
                                ['label' => 'Marcas', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/marca']),],
                                ['label' => 'Proveedor', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/proveedor']),],
                                ['label' => 'Producto por Proveedor', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/producto-proveedor']),],
                                ['label' => 'Temporada', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/temporada']),],
                                ['label' => 'Tipo de Producto', 'icon' => 'circle-o', 'url' => \yii\helpers\Url::to(['/maintenance/tipo-producto']),],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
