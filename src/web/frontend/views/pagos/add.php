<?php
/* @var $this yii\web\View */

use kartik\widgets\Select2;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;

$this->title                   = 'Pagos' ;
$this->params['breadcrumbs'][] = $this->title ;
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-lg-offset-4">
                            <?php $form = ActiveForm::begin([
                                    ]) ;
                            ?>
                            <label for="n_factura">Buscar N° Factura</label>
                            <?=
                            Select2::widget([
                                'name'    => 'n_factura',
                                'data'    => $data,
                                'size'    => Select2::MEDIUM,
                                'options' => [
                                    'placeholder' => 'Seleccione Factura ...',
                                ],
                                'addon'   => [
                                    'append' => [
                                        'content'  => Html::submitButton('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>', [
                                            'class'       => 'btn',
                                            'title'       => 'Buscar',
                                            'data-toggle' => 'tooltip'
                                        ]),
                                        'asButton' => true
                                    ]
                                ]
                            ]) ;
                            ?>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <hr>
                    <?php
                    if ($factura) :
                    ?>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                                <label>N° Factura</label>: 
                                <?= $factura->nro_factura ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <label>Proveedor</label>: 
                                <?php
                                $proveedor = app\models\Proveedor::findOne($factura->id_proveedor);
                                echo $proveedor['nombre'] ?>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <label>Monto Total de Factura</label>
                                <?php
                                $montoFactura = (isset($factura->monto_factura_final)) ? $factura->monto_factura_final : $factura->monto_factura_inicial ;
                                echo $montoFactura ;
                                ?>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <label>Estatus</label>
                                <?php
                                $estatus = common\models\EstatusFactura::findOne($factura->id_estatus);
                                echo $estatus['descripcion'] ?>
                            </div>
                        </div>
                    <hr>
                    <?php
                    endif ;
                    if ($cuotas) :
                    ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <label>Porcentaje de la cuota</label>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <label>Monto de la cuota</label>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                <label>Monto Abonado</label>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                <label>Monto Pendiente</label>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <label>Monto a Pagar</label>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <label>Fecha de pago</label>
                            </div>
                        </div>
                    <?php
                    $i = 0;
                    foreach ($cuotas as $value) :
                        $model[$value['id_cuota']] = $pago[$value['id_cuota']];
                        $i++;
                    ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <?= $value['porcentaje'] ?>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <?= $value['monto_estimado_pago'] ?>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                <?= $monto[$value['id_cuota']]['abonado'] ?>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                <?= $monto[$value['id_cuota']]['pendiente'] ?>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <?= 
                                \yii\helpers\Html::input('text','monto',null,['id_cuota_monto'=>$value['id_cuota'],'id'=>'monto','class'=> "form-control"]);
                                ?>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <?=
                                DatePicker::widget([
                                    'name' => 'fecha',
                                    'options' => [
                                        'id_cuota_fecha' => $value['id_cuota'],
                                        
                                        ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]);
                                ?>    
                            </div>
                            <div class="col-lg-2 col-md-1">
                                <?= \yii\helpers\Html::button('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>',['class'=>'btn btn-info cuota','id'=>'plus_cuota','id_cuota'=>$value['id_cuota']]) ?>
                            </div>
                        </div>
                    <br>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php
$url    = \yii\helpers\Url::to(['pagos/pay']);
$script = <<< JS
    $( ".cuota" ).bind( "click", function() {
        var id_cuota = $( this ).attr( "id_cuota" );
        var monto    = $( 'input[id_cuota_monto='+id_cuota+']' );
        var fecha    = $( 'input[id_cuota_fecha='+id_cuota+']' );
        
        if (fecha.val() == '') {
            alert("Debe ingresar Fecha");
        } else if (monto.val() == ''){
            alert("Debe ingresar Monto");
        } else {
            $.post("$url", { id_cuota:id_cuota,monto:monto.val(), fecha:fecha.val() } ,function(data){
                location.reload();
            });
        }
        
    });
JS;
$this->registerJs($script);
?>